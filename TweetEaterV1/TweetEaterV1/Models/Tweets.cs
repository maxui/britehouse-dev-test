﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TweetEaterV1.Models
{
    class Tweets
    {
        private string owner;
        private string message;

        /// <summary>
        /// Tweets the specified owner.
        /// </summary>
        /// <param name="owner">The owner.</param>
        /// <param name="message">The message.</param>
        public void Tweet(string owner, string message)
        {
            this.owner = owner;
            this.message = message;
        }

        /// <summary>
        /// Gets the owner.
        /// </summary>
        /// <returns></returns>
        public string getOwner()
        {
            return owner;
        }

        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <returns></returns>
        public string getMessage()
        {
            return message;
        }
    }
}
