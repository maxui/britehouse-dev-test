﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TweetEaterV1.Models
{
    class User
    {
        /// <summary>
        /// The name
        /// </summary>
        private string name;
        /// <summary>
        /// The followers
        /// </summary>
        private HashSet<string> followers;


        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <returns></returns>
        public string getName()
        {
            return name;
        }

        /// <summary>
        /// Sets the name.
        /// </summary>
        /// <param name="name">The name.</param>
        public void setName(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Gets the followers.
        /// </summary>
        /// <returns></returns>
        public HashSet<string> getFollowers()
        {
            return followers;
        }

        /// <summary>
        /// Sets the followers.
        /// </summary>
        /// <param name="followers">The followers.</param>
        public void setFollowers(HashSet<string> followers)
        {
            this.followers = followers;
        }


    }
}
