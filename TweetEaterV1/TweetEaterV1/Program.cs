﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using TweetEaterV1.Models;

namespace TweetEaterV1
{
    class Program
    {


        /// <summary>
        /// Mains the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        static void Main(string[] args)
        {
            if (args.Length != 2 || args == null)
            {
                Console.WriteLine("This app expects 2 files");
                Console.ReadKey();

            }

            else
            {
                Program.processFiles(args);
                Console.ReadKey();

            }
        }
        #region ProcessFiles
        /// <summary>
        /// Processes the files.
        /// </summary>
        /// <param name="args">The arguments.</param>
        private static void processFiles(string[] args)
        {
            try
            {
                if (!File.Exists(args[0]))
                {
                    throw new Exception(string.Format("Unable to find twitter user file: {0}", args[0]));
                }




                if (!File.Exists(args[1]))
                {
                    throw new Exception(string.Format("Unable to find twitterFeed file: {0}", args[1]));
                }
                //Do the Tweets
                List<Tweets> T = Program.loadTweets(args[1]).ToList();
                //DO the user
                List<User> U = Program.loadUsers(args[0]).OrderBy(x => x.getName()).ToList();



                // Do the Output
                foreach (User tweeter in U)
                {
                    Console.WriteLine(tweeter.getName().ToString());

                    foreach (Tweets tw in T)
                    {
                        string owner = tw.getOwner();

                        if (tweeter.getName().Equals(owner) || tweeter.getFollowers().Contains(owner))
                        {
                            Console.WriteLine(" @" + tw.getOwner().ToString() + ": " + tw.getMessage().ToString());
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }


        }
        #endregion
        #region loadTweets
        /// <summary>
        /// Loads the tweets.
        /// </summary>
        /// <param name="tweetsFile">The tweets file.</param>
        /// <returns></returns>
        public static List<Tweets> loadTweets(string tweetsFile)
        {

            List<Tweets> tweets = new List<Tweets>();

            try
            {
                // Create an instance of StreamReader to read from a file. 

                using (StreamReader sr = new StreamReader(tweetsFile))
                {
                    string line;
                    // Read and display lines from the file until the end of  
                    // the file is reached. 
                    while ((line = sr.ReadLine()) != null)
                    {

                        string[] values = line.Split(new string[] { "> " }, StringSplitOptions.None);
                        string user = values[0];
                        string message = values[1];

                        Tweets tweetObj = new Tweets();

                        tweetObj.Tweet(user, message);
                        tweets.Add(tweetObj);
                    }
                }
            }
            catch (Exception e)
            {
                // Let the user know what went wrong.
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }


            return tweets;
        }
        #endregion
        #region loadUsers
        /// <summary>
        /// Loads the users.
        /// </summary>
        /// <param name="usersFile">The users file.</param>
        /// <returns></returns>
        public static HashSet<User> loadUsers(string usersFile)
        {

            string Delimiter = "follows";
            Dictionary<string, User> tmpUsers = new Dictionary<String, User>();
            try
            {
                using (StreamReader sr = new StreamReader(usersFile))
                {
                    string line;
                    
                    while ((line = sr.ReadLine()) != null)
                    {
                        int i = line.IndexOf(Delimiter);
                        if (i < 0)
                        {
                            throw new Exception(string.Format("Malformed user file."));
                        }
                        string user = line.Substring(0, i).Trim();

                        if (line.Contains("follows"))
                        {
                            string followerCheck = line.Substring(i + Delimiter.Length).Trim();

                            if (followerCheck == null)
                            {
                                throw new Exception(string.Format("Malformed user file."));
                            }
                        }
                        string[] allFollowers = line.Substring(i + Delimiter.Length).Split(new string[] { "," }, StringSplitOptions.None);

                        User usr;
                        if (!tmpUsers.ContainsKey(user))
                        {
                            usr = new User();
                            usr.setName(user);
                            usr.setFollowers(new HashSet<String>());

                            tmpUsers.Add(user, usr);
                        }
                        else
                        {
                            usr = tmpUsers[user];
                        }

                        HashSet<string> followers = usr.getFollowers();
                        foreach (string thisFollower in allFollowers)
                        {
                            string follower = thisFollower.Trim();
                            followers.Add(follower);

                            User followerAcc;
                            if (!tmpUsers.ContainsKey(follower))
                            {
                                followerAcc = new User();
                                followerAcc.setName(follower);
                                followerAcc.setFollowers(new HashSet<string>());

                                tmpUsers.Add(follower, followerAcc);
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            HashSet<User> accounts = new HashSet<User>(tmpUsers.Values);

            return accounts;
        }
        #endregion




    }
}
